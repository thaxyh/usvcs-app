{{/*
Expand the name of the chart.
*/}}
{{- define "usvcs.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "usvcs.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "usvcs.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "usvcs.labels" -}}
helm.sh/chart: {{ include "usvcs.chart" . }}
{{ include "usvcs.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
"Tier" label
*/}}
{{- define "usvcs.tierLabel" -}}
app.kubernetes.io/tier: {{ .label }}
{{- end }}

{{/*

{{/*
"Name" label
*/}}
{{- define "usvcs.nameLabel" -}}
app.kubernetes.io/name: {{ include "usvcs.name" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "usvcs.selectorLabels" -}}
{{ include "usvcs.nameLabel" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "usvcs.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "usvcs.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
